import java.util.Calendar;
import java.util.GregorianCalendar;

public class RulesOf6005 {

	public static boolean hasFeature(String name){
		if (name.equalsIgnoreCase("lectures")||name.equalsIgnoreCase("recitations")||name.equalsIgnoreCase("text")||name.equalsIgnoreCase("problem sets")||name.equalsIgnoreCase("code review")||name.equalsIgnoreCase("returnin")||name.equalsIgnoreCase("projects")||name.equalsIgnoreCase("team meetings")||name.equalsIgnoreCase("quizzes")){
			return true;
		}
		return false;
	}

	public static int computeGrade(int quiz, int pset, int project, int participation){
		double grade = 0; int fgrade;
		grade+= (quiz * 0.2);
		grade+= (pset * 0.4);
		grade+= (project * 0.3);
		grade+= (participation * 0.1);
		/*
		double sub=grade%1;
		if(sub<.5){grade=(double)(Math.floor(grade));}
        if(sub>=.5){grade=(double)(Math.ceil(grade));}
       */
		grade=(double)(Math.round(grade));
		fgrade= (int)grade;
		return fgrade;  
	}
	
	
	/**
	 * Based on the slack day policy, returns a date of when the assignment would be due, making sure not to
	 * exceed the budget. In the case of the request being more than what's allowed, the latest possible
	 * due date is returned. 
	 * 
	 * Hint: Take a look at http://download.oracle.com/javase/6/docs/api/java/util/GregorianCalendar.html
	 * 
	 * Behavior is unspecified if request is negative or duedate is null. 
	 * 
	 * @param request - the requested number of slack days to use
	 * @param budget - the total number of available slack days
	 * @param duedate - the original due date of the assignment
	 * @return a new instance of a Calendar with the date and time set to when the assignment will be due
	 */
        public static Calendar extendDeadline(int request, int budget, Calendar duedate){
        		if(budget==0){}
        		else if(budget<=3){
					if(request>=budget){						
		                 duedate.add(Calendar.DATE, budget);
					}
					else if(request<budget){
		                 duedate.add(Calendar.DATE, request);
					}
				}
                else if(budget>3){
                	if(request>budget){
		                 duedate.add(Calendar.DATE, 3);
					}
					else if(request<budget && request>3){
		                 duedate.add(Calendar.DATE, 3);
					}
					else{
						
						duedate.add(Calendar.DATE, request);
					}
                }
               
				return duedate;
        }

        public static void main(String[] args){
            System.out.println("Has feature QUIZZES: " + hasFeature("QUIZZES"));
            System.out.println("My grade is: " + computeGrade(60, 40, 50, 37));
            Calendar duedate = new GregorianCalendar();
            duedate.set(2011, 8, 30, 23, 59, 59);
            System.out.println("Original due date: " + duedate.getTime());
            System.out.println("New due date:" + extendDeadline(4, 2, duedate).getTime());
    }
}