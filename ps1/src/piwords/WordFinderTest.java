package piwords;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class WordFinderTest {
    @Test
    public void basicGetSubstringsTest() {
        String haystack = "abcde";
        String[] needles = {"ab", "abc", "de", "fg"};

        Map<String, Integer> expectedOutput = new HashMap<String, Integer>();
        expectedOutput.put("ab", 0);
        expectedOutput.put("abc", 0);
        expectedOutput.put("de", 3);

        assertEquals(expectedOutput, WordFinder.getSubstrings(haystack,
                                                              needles));
        
        Map<String, Integer> Output = new HashMap<String, Integer>();
        String[] needles1 = {"cde", "bcd", "aa"};
        expectedOutput.put("cde", 2);
        expectedOutput.put("bcd", 1);
        assertEquals(Output, WordFinder.getSubstrings(haystack,
                                                              needles1));
        assertEquals(null, WordFinder.getSubstrings(null,
                needles1));
        assertEquals(null, WordFinder.getSubstrings(haystack,
                null));

    }

    // TODO: Write more tests (Problem 4.a)
}
