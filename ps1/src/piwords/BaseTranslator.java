package piwords;

public class BaseTranslator {
    /**
     * Converts an digits where the ith digit corresponds to (1 / baseA)^(i + 1)
     * digits[i], return an digits output of size precisionB where the ith digit
     * corresponds to (1 / baseB)^(i + 1) * output[i].
     * 
     * Stated in another way, digits is the fractional part of a number
     * expressed in baseA with the most significant digit first. The output is
     * the same number expressed in baseB with the most significant digit first.
     * 
     * To implement, logically, you're repeatedly multiplying the number by
     * baseB and chopping off the most significant digit at each iteration:
     * 
     * for (i < precisionB) {
     *   1. Keep a carry, initialize to 0.
     *   2. From RIGHT to LEFT
     *   	a. x = multiply the ith digit by baseB and add the carry
     *      b. the new ith digit is x % baseA
     *      c. carry = x / baseA
     *   3. output[i] = carry
     * 
     * If digits[i] < 0 or digits[i] >= baseA for any i, return null
     * If baseA < 2, baseB < 2, or precisionB < 1, return null
     * 
     * @param digits The input digits to translate. This digits is not mutated.
     * @param baseA The base that the input digits is expressed in.
     * @param baseB The base to translate into.
     * @param precisionB The number of digits of precision the output should
     *                   have.
     * @return An digits of size precisionB expressing digits in baseB.
     */
    public static int[] convertBase(int[] digits, int baseA,
                                    int baseB, int precisionB) {
        int output[]= new int[precisionB];
        int carry=0, x;
        if (baseA < 2 || baseB < 2 || precisionB < 1) return null;
              
        for(int k=0; k<precisionB; k++){ 
	        for(int i=digits.length-1; i>=0; i--){
		        	if(digits[i]<0||digits[i]>= baseA) return null;
		        	x= (baseB*digits[i]) +carry;	
		        	digits[i]= x%baseA;	        	
		        	carry= x/ baseA;
		        	output[k]= carry;
	        }
        }
        return output;
    }
}
