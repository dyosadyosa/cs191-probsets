package piwords;

import static org.junit.Assert.*;

import org.junit.Test;

public class BaseTranslatorTest {
    @Test
    public void basicBaseTranslatorTest() {
        // Expect that .01 in base-2 is .25 in base-10
        // (0 * 1/2^1 + 1 * 1/2^2 = .25)
        int[] input = {0, 1};
        int[] expectedOutput = {2, 5};
        assertArrayEquals(expectedOutput,
                          BaseTranslator.convertBase(input, 2, 10, 2));
        int[] a= {7,1};
        int[] a1={5,6};
        assertArrayEquals(a,
                BaseTranslator.convertBase(a1, 8, 10, 2));
        int[] b= {2,4};
        int[] b1={0,1,0,1};
        assertArrayEquals(b,
                BaseTranslator.convertBase(b1, 2, 8, 2));
        assertArrayEquals(null,
                BaseTranslator.convertBase(b1, 1, 8, 2));
        assertArrayEquals(null,
                BaseTranslator.convertBase(b1, 2, 1, 2));
        assertArrayEquals(null,
                BaseTranslator.convertBase(b1, 2, 8, 0));
        int[] a3={-1,6};
        int[] b3={0,1,0,2};
        assertArrayEquals(null,
                BaseTranslator.convertBase(b3, 2, 8, 2));
        assertArrayEquals(null,
                BaseTranslator.convertBase(a3, 2, 8, 2));
    }

    // TODO: Write more tests (Problem 2.a)
}
