package piwords;

import java.util.HashMap;
import java.util.Map;

public class WordFinder {
    /**
     * Given a String (the haystack) and an array of Strings (the needles),
     * return a Map<String, Integer>, where keys in the map correspond to
     * elements of needles that were found as substrings of haystack, and the
     * value for each key is the lowest index of haystack at which that needle
     * was found. A needle that was not found in the haystack should not be
     * returned in the output map.
     * 
     * @param haystack The string to search into.
     * @param needles The array of strings to search for. This array is not
     *                mutated.
     * @return The list of needles that were found in the haystack.
     */
    public static Map<String, Integer> getSubstrings(String haystack,
                                                    String[] needles) { 
    	int i , x, j, length;
    	Map<String, Integer> output = new HashMap<String, Integer>();
    	if(haystack==null||needles==null) return null;
    	for( i = 0; i<needles.length; i++){
    		length = needles[i].length();
    		j = 0;
    		for(x = 0; x<needles.length-length; x++){
    			j = haystack.indexOf(needles[i], x);
    			if(j>=0){
        			output.put(needles[i], j);
        			break;
        		}
    		}
    		
    	}
    	
    	return output;
    }
}
