package piwords;

import static org.junit.Assert.*;

import org.junit.Test;

public class PiGeneratorTest {
    @Test
    public void basicPowerModTest() {
 
        assertEquals(17, PiGenerator.powerMod(5, 7, 23));
        assertEquals(8, PiGenerator.powerMod(6, 3, 13));
        assertEquals(1, PiGenerator.powerMod(8, 4, 15));
        
        int output[] = {2, 4, 3, 15, 6, 10, 8, 8, 8, 5};
        assertArrayEquals(output, PiGenerator.computePiInHex(10));
        int output1[] = {2, 4, 3, 15, 6, 10, 8, 8, 8};
        assertArrayEquals(output1, PiGenerator.computePiInHex(9));
    }
        
}
